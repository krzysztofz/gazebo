
Projekt zrobiony w Ros Developement Studio
Aby uruchomić robota należy w studiu włączyć gazebo
następnie za pomocą komendy:
roslaunch m2wr_description spawn.launch
dodać robota.
Kolejnym krokiem jest wybranie algorytmu sterowania.
Są dwa algorytmy, z czego jeden zapamiętuje poprzednie stany, a
drugi nie. Ten pierwszy działa płynniej i mapa rysowana na jego podstawie jest dokładniejsza.
rosrun motion_plan algorytm.py - pierwszy algorytm
rosrun motion_plan algorytm1.py - drugi algorytm
Zapisują one dane ze sterowania odpowiednio do plików trasa3.txt
i trasa.txt.
Rysowanie mapy zachodzi poza ros developement studio.
Należy przekopiować zawartość pliku z danymi i program rysowanie.py (dla algorytm1.py) 
lub rysowanie_na_podstawie_decyzji.py (dla algorytm.py) narysują za pomocą biblioteki myplotlib trasę labiryntu.