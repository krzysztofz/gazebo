#! /usr/bin/env python

import rospy
import matplotlib.pyplot as plt

from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
pub = None

trasa=[[0,0]]

def clbk_laser(msg):
    a1=round(min(msg.ranges[0:119]),2)
    a2=round(min(msg.ranges[120:239]),2)
    a3=round(min(msg.ranges[240:359]),2)
    a4=round(min(msg.ranges[360:479]),2)
    a5=round(min(msg.ranges[480:599]),2)
    a6=round(min(msg.ranges[600:719]),2)
    regions = {
        '1':min(a1, 10),
        '2':min(a2, 10),
        '3':min(a3, 10),
        '4':min(a4, 10),
        '5':min(a5, 10),
        '6':min(a6, 10),
    }


    take_action(regions)

def take_action(regions):
    msg = Twist()
    linear_x = 0
    angular_z = 0

    state_description = str(regions)
    # if regions['1']==10 and regions['1']==10 and regions['3']==10 and regions['4']==10 and regions['6']==10:

    #     f=open("catkin_ws/src/two-wheeled-robot-motion-planning/scripts/trasa.txt","w+")
    #     f.write(str(trasa))
    #     f.close()

    if regions['1'] <1.15 and regions['6'] < 1.15 :
        state_description += 'do przodu'

        linear_x = 0.3
        if regions['3']<0.4 and regions['4']<0.4:
            state_description+="sciana"
            angular_z=-1.5
        elif regions['1']>regions['6']+0.3:
            angular_z = 0.1
        elif regions['1']<regions['6']-0.3:
            linear_x = 0
            angular_z=-0.1

        elif regions['3']<0.4 and regions['4']<0.4:
            state_description+="sciana"
            angular_z=-1.5
        else:
            angular_z = 0
    elif regions['1']>1:
        state_description += 'w prawo'
        linear_x = 0.1
        if regions['1']>4:
            angular_z = 1.7
        elif regions['1']>2:
            angular_z = 1.3
        elif regions['1']>1.7:
            angular_z = 0.8
        else:
            angular_z=0.4
    elif regions['6']>1 and regions['1']<regions['6']+0.2:
        state_description += 'w lewo'
        linear_x = 0.1
        if regions['6']>4:
            angular_z = -2
        elif regions['6']>2:
            angular_z = -1.2
        elif regions['6']>1.7:
            angular_z = -0.6
        else:
            angular_z=-0.4



    else:
        state_description += 'unknown case'
        #rospy.loginfo(regions)
        linear_x = 0.2
    rospy.loginfo(regions)
    w1=round(linear_x,2)
    w2=round(angular_z,2)
    trasa.append([trasa[-1][0]+w1,trasa[-1][1]+w2])
    rospy.loginfo(state_description)
    f=open("catkin_ws/src/two-wheeled-robot-motion-planning/scripts/trasa.txt","a+")
    f.write(str(trasa[-1]))
    f.write("\n")
    f.close()
    msg.linear.x = linear_x
    msg.angular.z = angular_z
    pub.publish(msg)

def main():
    global pub
    open("catkin_ws/src/two-wheeled-robot-motion-planning/scripts/trasa.txt","w").close()
    rospy.init_node('reading_laser')

    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

    sub = rospy.Subscriber('/m2wr/laser/scan', LaserScan, clbk_laser)

    rospy.spin()

if __name__ == '__main__':
    main()
