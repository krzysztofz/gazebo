#! /usr/bin/env python

import rospy
import matplotlib.pyplot as plt

from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
pub = None

trasa=[[0,0]]
korytarz=[]
dzialanie=[0,0,0,0,0,0,0,0]
cofanie=[0]
#1-prawo
#0-prosto
#-1-lewo
#1000-zawrot


def clbk_laser(msg):
    a1=round(min(msg.ranges[0:102]),2)
    a2=round(min(msg.ranges[103:205]),2)
    a3=round(min(msg.ranges[206:307]),2)
    a4=round(min(msg.ranges[308:409]),2)
    a5=round(min(msg.ranges[410:511]),2)
    a6=round(min(msg.ranges[512:614]),2)
    a7=round(min(msg.ranges[615:719]),2)
    regions = {
        '1':min(a1, 10),
        '2':min(a2, 10),
        '3':min(a3, 10),
        '4':min(a4, 10),
        '5':min(a5, 10),
        '6':min(a6, 10),
        '7':min(a7, 10),

    }


    take_action(regions)

def take_action(regions):
    msg = Twist()
    linear_x = 0
    angular_z = 0

    state_description = str(regions)
    # if regions['1']==10 and regions['1']==10 and regions['3']==10 and regions['4']==10 and regions['6']==10:

    #     f=open("catkin_ws/src/two-wheeled-robot-motion-planning/scripts/trasa.txt","w+")
    #     f.write(str(trasa))
    #     f.close()
    sytuacja=[]
    if(regions['7']>2):
        sytuacja.append(1)
    else:
        sytuacja.append(0)
    if(regions['4']>0.8 ):
        sytuacja.append(1)
    else:
        sytuacja.append(0)
    if(regions['1']>2):
        sytuacja.append(1)
    else:
        sytuacja.append(0)
    korytarz.append(sytuacja)
    suma=sum(regions.values())
    kom=''
    if(suma>55):
        kom+='poza labiryntem'
        linear_x=0
        angular_z=0
    elif (regions['3']<0.4 or cofanie[-1]>1 and cofanie[-1]<10):
        kom+='cofanie '
        linear_x=-0.3
        cofanie.append(cofanie[-1]+1)
    elif((sytuacja[2]>0 or dzialanie[-1]>0) and dzialanie[-1]<900):
        kom+='prawo '
        cofanie.append(0)
        dzialanie.append(dzialanie[-1]+1)
        if(dzialanie[-1]>0 and dzialanie[-1]<5):
            linear_x=0.3
        if(dzialanie[-1]>5 and dzialanie[-1]<95):
            linear_x=0.3
            angular_z=0.5
        elif(dzialanie[-1]>100 and dzialanie[-1]<900):
            dzialanie.append(0)

    elif(sytuacja[1]>0 and dzialanie[-1]<900 and dzialanie[-1]>-1):
        cofanie.append(0)
        dzialanie.append(0)
        kom+='przod '
        linear_x=0.3
        if(regions['1']+0.4<regions['7']):
             kom+=' korekta w lewo '
             angular_z=-0.1
        if(regions['7']+0.4<regions['1']):
             kom+=' korekta w prawo '
             angular_z=+0.1
    elif((sytuacja[0]>0 or dzialanie[-1]<0)and dzialanie[-1]<900):
        cofanie.append(0)
        kom+='lewo '
        dzialanie.append(dzialanie[-1]-1)
        if(dzialanie[-1]<0 and dzialanie[-1]>-10):
            linear_x=-0.4
        if(dzialanie[-1]<-10 and dzialanie[-1]>-100):
            linear_x=0.3
            angular_z=-0.5
        elif(dzialanie[-1]<-100):
            dzialanie.append(0)
    elif((sytuacja[0]==0 and sytuacja[1]==0 and sytuacja[2]==0 ) or dzialanie[-1]>900):
        cofanie.append(0)
        kom+='zawrot '
        dzialanie.append(dzialanie[-1]+1000)
        if(dzialanie[-1]>150 and dzialanie[-1]<100000):
            angular_z=1
            linear_x=0.1
        if(dzialanie[-1]>100000):
            dzialanie.append(0)



    rospy.loginfo(regions)
    w1=round(linear_x,2)
    w2=round(angular_z,2)
    trasa.append([trasa[-1][0]+w1,trasa[-1][1]+w2])
    rospy.loginfo(korytarz[-1])
    rospy.loginfo(kom+str(dzialanie[-1]))
    f=open('catkin_ws/src/two-wheeled-robot-motion-planning/scripts/trasa3.txt',"a+")
    f.write(str(trasa[-1])+str(dzialanie[-1]))
    f.write("\n")
    f.close()
    msg.linear.x = linear_x
    msg.angular.z = angular_z
    pub.publish(msg)

def main():
    global pub
    open('catkin_ws/src/two-wheeled-robot-motion-planning/scripts/trasa3.txt', 'w').close()

    rospy.init_node('reading_laser')

    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)

    sub = rospy.Subscriber('/m2wr/laser/scan', LaserScan, clbk_laser)

    rospy.spin()

if __name__ == '__main__':
    main()
